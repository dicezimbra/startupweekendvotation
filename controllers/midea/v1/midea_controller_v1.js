var path_root = "../../../";
var global = require('../../../server.js');
var utils = require(path_root+'controllers/util/utils');

var MIdea = require(path_root+'models/MIdea');
var MStartup = require(path_root+'models/MStartup');
var MUser = require(path_root+'models/MUser');
var fs = require('fs');
var path = require('path');

var DEBUG = true;

/**
 * get /user  
 *
 * new signup or login
 */

exports.clear = function(req , res){
	var v = req.query.v;
	var p1_swid = req.query.p1;
	var p2_apagarsw = req.query.p2;
	var resultFunction = function(cod,msg){		

		var result = new function(){
			this.v = 1;
			this.cod = cod;
			this.msg = msg;	
		};

		res.end(JSON.stringify({
			ret: result
		}));

	};

	if(p1_swid&&p2_apagarsw){
		MIdea.remove({swid : p1_swid}, function(err, obj){

			if(err){
				return resultFunction(2,"erro ao apagar");
			}

			MUser.remove({}, function(err, obj){
				if(err){
					return resultFunction(3,"erro ao apagar");
				}

				return resultFunction(0,"apagou com sucesso");
			});
		});
	}

	else if(p1_swid){
		MStartup.remove({_id: p1_swid}, function(err, obj){

				if(err){
					return resultFunction(1,"erro ao apagar");
				}


				MIdea.remove({swid : p1_swid}, function(err, obj){

					if(err){
						return resultFunction(2,"erro ao apagar");
					}

					MUser.remove({}, function(err, obj){
						if(err){
							return resultFunction(3,"erro ao apagar");
						}

						return resultFunction(0,"apagou com sucesso");
					});
				});

		});
		
	}

	MUser.remove({}, function(err, obj){
		if(err){
			return resultFunction(3,"erro ao apagar");
		}

		return resultFunction(0,"apagou com sucesso");
	});
	
}


exports.add_idea = function(req, res) {
	console.log('add ideia');
	

	var param = req.body;
	console.log('add_sw param = '+param);
	if(!param || !param.p1 || !param.p2 || !param.p3 || !param.p4){
		return res.send(500);
	}

  
	var p1_name = param.p1;
	var p2_description =param.p2;
	var p3_username = param.p3;
	var p4_swid = param.p4;

 	if(p4_swid == '571bc5c9eb65be9c600751ad'){
		p4_swid = '5723675cddc06d6ab06f14b6';
	}
	
	var resultFunction = function(cod,msg){		

		var result = new function(){
			this.v = 1;
			this.cod = cod;
			this.msg = msg;	
		};

		// JSON
		res.end(JSON.stringify({
			ret: result
		}));

	};

	if(!p1_name){
		return resultFunction(-100,"Invalid param");
	}
	if(!p2_description){
		return resultFunction(-100,"Invalid param");
	}
	if(!p3_username){
		return resultFunction(-101,"Invalid param");
	}
	if(!p4_swid){
		return resultFunction(-101,"Invalid param");
	}

	


	MStartup.findOne({_id: p4_swid}, function(err, obj) {	
			if(err){ 
				resultFunction(-1,"Error :"+err);
				return;
			}

			if(obj){


				MIdea.findOne({name: p1_name}, function(err, obj) {	
     
		        console.log('obj='+obj);
		        
				if(err){ 
					resultFunction(-1,"Error :"+err);
					return;
				}

				if(obj){
		           resultFunction(-1,"Ja existe essa ideia");
				}else{

					obj = new MIdea();

					obj.name = p1_name;
					obj.description = p2_description;
		            obj.username = p3_username;
					obj.time = new Date();
					obj.swid = p4_swid;
					obj.votos = 0;
					

					obj.save(function(err) {
						if (err) {
							resultFunction(-3,"Error :"+err);
							return;      
						} 


						MIdea.find({swid: p4_swid}, function(err, ideias){
							global.io.sockets.emit('newIdea', ideias);
						});

						
						resultFunction(0,""+obj._id);   
					});
				}
			});

			}else{
				 resultFunction(-1,"Sw nao criado");
			}
	});
};


exports.index = function(req, res){

	res.render('apuracao', { title: 'Express' });
};
/**
 * get /user  
 *
 * new signup or login
 */
exports.get_idea = function(req, res) {

	var v = req.query.v;
  
	var p4_swid = req.query.p1;
 	if(p4_swid == '571bc5c9eb65be9c600751ad'){
		p4_swid = '5723675cddc06d6ab06f14b6';
	}
	var resultFunction = function(cod,msg){		

		var result = new function(){
			this.v = 1;
			this.cod = cod;
			this.msg = msg;	
		};

		// JSON
		res.end(JSON.stringify({
			ret: result
		}));

	};

	var _buff = [];
	
	MStartup.findOne({_id: p4_swid}, function(err, obj) {	
			if(err){ 
				resultFunction(-1,"Error :"+err);
				return;
			}

			if(!obj){
				 resultFunction(-1,"Sw nao criado");

			}
			MIdea.find({swid: p4_swid}, function(err, post) {	
				     
			        console.log('obj='+post);
			        
					if(err){ 
						resultFunction(-1,"Error :"+err);
						return;
					}

					if(!post){
						resultFunction(-1,"Não há ideias :"+err);
						return;
					}
			           
					if(post.length==0){
						resultFunction(-1,"Não há ideias :"+err);
						return;
					}

					for (var i = 0; i < post.length; i++) {	
					
		                    var obj = post[i];
		                    console.log('v2_posts_search obj.date = '+obj.date);
		                    
		                   //console.log('v2_posts_search obj.date = '+obj.date.getTime());
		                   
							var objJson = new function(){
		                        this.id = obj._id;
		                        this.name = obj.name;
		                        this.description = obj.description;
		                        this.username = obj.username;
		                        this.time = obj.time;
		                        this.swid = obj.swid;
		                        this.votos = ""+obj.votos;
		                      
		                    };
							_buff.push(objJson);
												
					};
	            
	            	return resultFunction(0,_buff);
			});
	});
};

exports.apuracao = function(req, res) {

  
	var p1_swid = req.query.p1;
	if(p1_swid == '571bc5c9eb65be9c600751ad'){
		p1_swid = '5723675cddc06d6ab06f14b6';
	}
 	console.log('midea_controller_v1 apuracao p1_swid = '+p1_swid);
	var resultFunction = function(cod,msg){		

		var result = new function(){
			this.v = 1;
			this.cod = cod;
			this.msg = msg;	
		};

		// JSON
		res.end(JSON.stringify({
			ret: result
		}));

	};

	var _buff = [];
	console.log('midea_controller_v1 MStartup.findOne ');
	MStartup.findOne({_id: p1_swid}, function(err, obj) {	
			
			
				if(err){ 
					resultFunction(-1,"Error :"+err);
					return;
				}

				if(!obj){
					 resultFunction(-1,"Sw nao criado");
					 return;
				}
			

			console.log('midea_controller_v1 sw encontrado');
			MIdea.find({swid: p1_swid}).sort({ votos: -1 }).exec( function(err, obj){

					if(err){ 
						resultFunction(-1,"Error :"+err);
						return;
					}

					if(!obj){
						resultFunction(-1,"Não há ideias :"+err);
						return;
					}
					console.log('midea_controller_v1 MIdea encontrado MIdea = '+obj);
	            	return resultFunction(0,obj);

			});
	});
};

