var path_root = "../../";

var utils = require(path_root+'controllers/util/utils');




/**
 * GET /
 * index page.
 */
exports.index = function(req, res) {
	res.send(200);
};

/**
 * get /v1/app/privacy_policy
 */
exports.get_app_privacy_policy= function(req, res){	
console.log('get_android_version');  
var text = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></head><body><b>Politica de Privacidade</b><br><br>O Cloff possui o compromisso com a privacidade e a segurança de seus participantes durante todo o processo de navegação em nossos aplicativos.<br><br>Ao clicar no botão login você será direcionado para uma pagina de identificação do Facebook, e logo após realizado o login você será redirecionado para nosso aplicativo, portanto não possuímos e nem armazenamos seu login e senha do Facebook.<br><br>Usamos o seu endereço de e-mail e o seu perfil no Facebook para enviar comunicados a você sobre nossos serviços além de garantir as melhores ofertas e promoções para você. Durante todo este processo mantemos suas informações em sigilo absoluto.<br><br>Tomamos medidas de segurança adequadas para nos proteger contra acesso, alteração, divulgação ou destruição não autorizada dos dados.<br><br>As fotos são publicadas pelos usuários e, diariamente, realizamos uma moderação relacionada ao conteúdo das imagens, visando bloquear imagens inadequadas. A qualquer momento é possível denunciar uma imagem com conteúdo impróprio, clicando no botão de moderação de imagem.<br><br>Publicaremos todas as alterações da Política de Privacidade nesta página e, se as alterações forem significativas, colocaremos um aviso com mais destaque, incluindo, para alguns serviços, notificação por e-mail das alterações da Política de Privacidade.<br><br>Equipe Cloff.<br><br>22 de Abril de 2015</body></html>';
res.charset = 'utf8';
res.writeHead(200, {'Content-Type': 'text/html; charset=UTF-8'});
res.write(text);
res.end(); 
/*
O Cloff possui o compromisso com a privacidade e a segurança de seus participantes durante todo o processo de navegação em nossos aplicativos.

Ao clicar no botão login você será direcionado para uma pagina de identificação do Facebook, e logo após realizado o login você será redirecionado para nosso aplicativo, portanto não possuímos e nem armazenamos seu login e senha do Facebook.

Usamos o seu endereço de e-mail e o seu perfil no Facebook para enviar comunicados a você sobre nossos serviços além de garantir as melhores ofertas e promoções para você. Durante todo este processo mantemos suas informações em sigilo absoluto.

Tomamos medidas de segurança adequadas para nos proteger contra acesso, alteração, divulgação ou destruição não autorizada dos dados. Essas medidas incluem análises internas de nossas práticas de coleta, armazenamento e processamento de dados e medidas de segurança, incluindo criptografia e medidas de segurança física apropriadas para nos proteger contra o acesso não autorizado a sistemas em que armazenamos os dados pessoais.

Para que estes dados permaneçam intactos, desaconselhamos expressamente a divulgação de sua senha a terceiros, mesmo a amigos e parentes.

As fotos são publicadas pelos usuários e, diariamente, realizamos uma moderação relacionada ao conteúdo das imagens, visando bloquear imagens inadequadas. A qualquer momento é possível denunciar uma imagem com conteúdo impróprio, clicando no botão de moderação de imagem.

Publicaremos todas as alterações da Política de Privacidade nesta página e, se as alterações forem significativas, colocaremos um aviso com mais destaque, incluindo, para alguns serviços, notificação por e-mail das alterações da Política de Privacidade.

Equipe Cloff.
*/
};

