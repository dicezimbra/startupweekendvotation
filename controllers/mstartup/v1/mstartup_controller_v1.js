var path_root = "../../../";

var utils = require(path_root+'controllers/util/utils');
var MStartup = require(path_root+'models/MStartup');


var fs = require('fs');
var path = require('path');


var DEBUG = true;

/**
 * post /post
 *
 * new post
 */

exports.get_all = function(req , res){
	MStartup.find({}, function(error, startup) {
		if(error) res.send(500);

		res.json({ startup: startup });
	});
}
var conexoes;
exports.index = function(req, res){
	conexoes++;
	console.log(conexoes);
	res.render('index', { title: 'Express' });
};




exports.add_sw = function(req, res) {
	console.log('add_sw');
	var resultFunction = function(cod,msg){
		var result = new function(){
			this.v = 1;
			this.cod = cod;
			this.msg = msg;	
		};
        
		// JSON
		res.end(JSON.stringify({
			ret: result
		}));
	};

	

	var param = req.body;
	console.log('add_sw param = '+param);
	if(!param || !param.p1 || !param.p2){
		return res.send(500);
	}

	var p2_name = param.p1;
	var p1_location = param.p2;
	console.log('add_sw p2_name = '+p2_name);
	console.log('add_sw p1_location = '+p1_location);

	if(!p1_location || !p2_name){
		return res.send(501);
	}
   
	
	MStartup.findOne({location: p1_location, name: p2_name}, function(err, obj) {	
    

        console.log('obj='+obj);
        
		if(err){ 
			resultFunction(-1,"Error :"+err);
			return;
		}


		if(!obj){

			obj = new MStartup();
			obj.location = p1_location;
			obj.year = new Date();
			obj.name = p2_name;
			obj.votation = false;
			obj.idea = false;
		
			obj.save(function(err) {
				if (err) {
					resultFunction(-3,"Error :"+err);
					return;      
				} 
					
				resultFunction(0,""+obj._id);   
			});
		}else{
			resultFunction(-1,"Já existe com mesmo nome e local");
			return;
		}
	});
};

