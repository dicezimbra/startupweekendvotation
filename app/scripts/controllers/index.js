'use strict';

/**
 * @ngdoc function
 * @name startupApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the startupApp
 */

angular.module('startupApp')
  .controller('IndexCtrl', function ($scope, $http, config) {
   


	function MStartup() {
		this.p1 = '';
		this.p2 = '';
	}
	

	$scope.startup = new MStartup();
	$scope.startups = [];
	$scope.baseUrl = config.baseUrl;

	$http.get(config.baseUrl + '/v1/getsws').success(function(retorno) {
		$scope.startups = retorno.startup;
	});

	$scope.addSw = function() {
		$http.post(config.baseUrl + '/v1/newsw', $scope.startup).success(function() {
			$scope.startups.push($scope.startup);
			$scope.startup = new MStartup();
		});
	}

  });
