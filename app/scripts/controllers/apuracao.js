'use strict';

/**
 * @ngdoc function
 * @name startupApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the startupApp
 */
angular.module('startupApp')
  .controller('ApuracaoCtrl', function ($http, $scope, $routeParams, config) {
   
	
	$scope.ideias = [];

	$http.get(config.baseUrl + '/v1/getidea?p1='+$routeParams.id).success(function(retorno) {
		$scope.ideias = retorno.ret.msg;
	});

	this.socket = io.connect(config.baseUrl);

	this.socket.on('newVote', function (ideias) {
	   	$scope.ideias = ideias;
	   	$scope.$apply();
	});
  });
