'use strict';

/**
 * @ngdoc function
 * @name startupApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the startupApp
 */

angular.module('startupApp')
  .controller('IdeiaCtrl', function ($scope, $http, config, $routeParams) {
   
	function MIdeia() {
		this.p1 = '';
		this.p2 = '';
		this.p3 = '';
		this.p4 = '';
	}
	
	$scope.id = $routeParams.id;
	$scope.ideia = new MIdeia();
	$scope.ideias = [];
	$scope.baseUrl = config.baseUrl;


	$http.get(config.baseUrl + '/v1/getidea?p1='+$routeParams.id).success(function(retorno) {
		$scope.ideias = retorno.ret.msg;
	});

	this.socket = io.connect(config.baseUrl);

	this.socket.on('newIdea', function (ideias) {
	   	$scope.ideias = ideias;
	   	$scope.$apply();
	});

	$scope.addIdeia = function() {
		console.log('add ideia $scope.id = '+$scope.id);
		$scope.ideia.p4 = $scope.id;
		console.log('add ideia $scope.id = '+$scope.id);

		$http.post(config.baseUrl + '/v1/adicionarsdeia/', $scope.ideia).success(function() {
			$scope.ideia = new MIdeia();
		});
	}

  });
