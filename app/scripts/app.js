'use strict';

angular
  .module('startupApp', [
    'ngAnimate',
    'ngRoute',
    'ngSanitize'
  ])
  .config(function ($routeProvider, $httpProvider, $locationProvider) {
    $httpProvider.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    });
    $routeProvider
      .when('/', {
        templateUrl: '/views/index.html',
        controller: 'IndexCtrl'
       
      })
      .when('/apuracao/:id', {
        templateUrl: '/views/apuracao.html',
        controller: 'ApuracaoCtrl'
      })
       .when('/ideia/:id', {
        templateUrl: '/views/ideia.html',
        controller: 'IdeiaCtrl'
      });
  });
