var app_name = "cezimbrabot";
/**
 * Module dependencies.
 */
var express = require('express');
var MongoStore = require('connect-mongo')(express);
var path = require('path');
var mongoose = require('mongoose');
var expressValidator = require('express-validator');
var http = require('http');


/**
 * Create Express server.
 */
var app = express();

var dbHost = process.env.OPENSHIFT_MONGODB_DB_URL || "mongodb://localhost:27017/";
var dbName = process.env.OPENSHIFT_APP_NAME       || app_name;
var dbUrl = "" + dbHost + dbName;

/**
 * Mongoose configuration.
 */

mongoose.connect(dbUrl);
mongoose.connection.on('error', function() {
	console.error('MongoDB Connection Error. Please make sure MongoDB is running.');
});

/**
 * Express configuration.
 */

var hour = 3600000;
var day = (hour * 24);
var week = (day * 7);
var month = (day * 30);

var ipaddr = process.env.OPENSHIFT_NODEJS_IP || "0.0.0.0";
var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 3000;

//all environments
app.set('port', port);
app.set('ipaddr', ipaddr);

//app.set('view engine', 'html');

app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.cookieParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static('./app'));

app.use(express.session({
	secret: "secret of my backend app "+app_name,
	cookie: { maxAge: day },
	store: new MongoStore({
		db: mongoose.connection.db,
		clear_interval: 3600,
		auto_reconnect: true
	})
}));
app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
app.all('*', function(req, res, next) {
 	
    origin = 'http://swinatel-cezimbra.rhcloud.com/';

    res.header('Access-Control-Allow-Origin', origin);

    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With , Access-Control-Allow-Origin, Content-Type, Accept');
    next();
});




//app.enable('trust proxy');

/**
 * Load controllers V1.
 */
var mstartup_controller_v1 = require('./controllers/mstartup/v1/mstartup_controller_v1');
var midea_controller_v1 = require('./controllers/midea/v1/midea_controller_v1');
var muser_controller_v1 = require('./controllers/muser/v1/muser_controller_v1');
var mfront_controller_v1 = require('./controllers/mfront/v1/mfront_controller_v1');

var general = require('./controllers/util/general');

/****************************************************************
 * 		HOME PAGE
 ***************************************************************/
//  get






/**************************************************************
 * 	MOBILE		 			MPost - V1                                  
 **************************************************************/  
//  get
app.post('/v1/adicionarsdeia', midea_controller_v1.add_idea); 
app.post('/v1/newsw', mstartup_controller_v1.add_sw); 
app.get('/v1/getsws', mstartup_controller_v1.get_all);  

app.get('/v1/a', midea_controller_v1.index); 

app.get('/v1/getidea', midea_controller_v1.get_idea);
app.get('/v1/apuracao', midea_controller_v1.apuracao);
app.get('/v1/clear', midea_controller_v1.clear);

app.get('/v1/votation', muser_controller_v1.votation);

app.get('/v1/home', mfront_controller_v1.index);
app.post('/v1/contato', mfront_controller_v1.adicionaContato);
app.get('/v1/contatos', mfront_controller_v1.listaContatos);




/****************************************************************
 * 		INICIANDO SERVIDOR
 ***************************************************************/

/**
 * Start Express server.
 */
var server = http.createServer(app).listen(app.get('port'), app.get('ipaddr'), function(){
    
	console.log('%s: Express server started on %s:%d ...',
			Date(Date.now() ), app.get('ipaddr'), app.get('port') );
});
var io = require('socket.io')(server);

exports.io = io;
