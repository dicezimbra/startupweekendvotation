var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
  id: { type: String},
  name: { type: String, unique: true },
  description: { type: String, default: '' },
  username: String,
  time : Date,
  swid : String,
  votos : Number
});

module.exports = mongoose.model('MIdea', userSchema);
