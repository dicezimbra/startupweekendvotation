var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
  id: { type: String},
  location: { type: String},
  year: { type: String, default: '' },
  name: String,
  votation: Boolean,
  idea: Boolean
  
});

module.exports = mongoose.model('MStartup', userSchema);
