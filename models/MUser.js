var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({

  id: { type: String},
  free_votation: Boolean,
  device_id: String,
  votos_restantes:Number,
  votos: String
});

module.exports = mongoose.model('MUser', userSchema);
